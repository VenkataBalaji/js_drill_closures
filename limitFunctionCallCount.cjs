function limitFunctionCallCount(cb, n) {
   
    let count = 0;
    return function(){
        if(count<n){
            cb();
            count++;
        }
        else{
            console.log('call count limit reached');
        }
    }

  }
  
  function cb()
  {
      console.log('callback');
  }
  
  module.exports = { limitFunctionCallCount, cb }