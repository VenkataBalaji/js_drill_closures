function counterFactory(){
    let counter=1;
    return {
        increment:()=>{
             return counter++
        },
        decrement:()=>{
            return counter--
        }
    }
}


module.exports=counterFactory

