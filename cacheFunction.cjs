function catheFUnction(cb){
    let cache={};
    return function(a,b){
        let key=`${a},${b}`
        if(key in cache){
            console.log('already present');
            return cache[key];
        }else{
            let result=cb(a,b);
            cache[key]=result;
            return result
        }
    }
}

function sum(a,b){
    console.log('adding....');
    return a+b
}


let letsadd2numbers = catheFUnction(sum);

console.log(letsadd2numbers(1,2));
console.log(letsadd2numbers(10,20));
console.log(letsadd2numbers(1,2));
console.log(letsadd2numbers(10,20));




module.exports=catheFUnction